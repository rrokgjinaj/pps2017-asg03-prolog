name := "pps2017-asg03-prolog"

version := "0.1"

scalaVersion := "2.12.8"

libraryDependencies  += "it.unibo.alice.tuprolog" % "tuprolog" %"2.1.1"
// https://mvnrepository.com/artifact/org.json/json
libraryDependencies += "org.json" % "json" % "20180813"
libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.8"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.8" % "test"
