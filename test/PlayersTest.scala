import org.json.JSONArray
import org.scalatest.FlatSpec

class PlayersTest extends FlatSpec{
  Assignment3.initializeProlog()
  var players = Assignment3.getChanpionsPlayer()

  "This Players" should "not be the champions player" in {
    assert(List("Robert Lewandowski", "Mats Hummels", "Franck Kessié") != players)
  }
  "The Champions List" should "contain Alisson Becker" in {
    assert(players.contains("Alisson Becker"))
  }

  "Xherdan Shaqiri" should "must Have Won" in {
    assert(players.contains("Xherdan Shaqiri"))
  }

  "Team of Xherdan Shaqiri" should "have two Teams" in {
    assert(Assignment3.getPlayerTeams("Xherdan Shaqiri") == List("bayern", "liverpool"))
  }

  "Soccer Data " should "be JSONArray Type" in {
    assert(Assignment3.soccerData.isInstanceOf[JSONArray] )
  }

  "Player teams " should "be List[String] Type" in {
    assert(Assignment3.getPlayerTeams("Mohamed Salah").isInstanceOf[List[String]] )
  }

  "Team of Xherdan Shaqiri computet with prolog" should "be the same computed in scala" in {
    assert(Assignment3.getPlayerTeams("Xherdan Shaqiri") == Assignment3.getPlayerTeamsProlog("Xherdan Shaqiri"))
  }

  "Team of Mohamed Salah computet with prolog" should "be the same computed in scala" in {
    assert(Assignment3.getPlayerTeams("Mohamed Salah") == Assignment3.getPlayerTeamsProlog("Mohamed Salah"))
  }
}

