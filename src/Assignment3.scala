import java.io.{File, FileInputStream, FileReader, IOException}

import alice.tuprolog.{Engine, Prolog, Term, Theory}
import org.json.{JSONArray, JSONObject, JSONTokener}

import scala.collection.mutable.ListBuffer

object Assignment3 extends App {

  var soccerData: JSONArray = _
  var engine: Prolog = _
  var jsonInputFile: String = _
  var assignment3: File = _
  var theory:Theory = _

  /**
    *
    */
  def initializeProlog() : Unit = {
    engine = new Prolog
    jsonInputFile = "src/datas.json"
    assignment3 = new File("src/assignment3.pl")
    theory = new Theory(new FileInputStream(assignment3))
    soccerData = new JSONArray(new JSONTokener(new FileReader(jsonInputFile)))

    for(
      yearData <- soccerData if yearData.getInt("anno") > 2010;
      year = yearData.getInt("anno");
      teamData <- yearData.getJSONArray("squadre");
      teamName = teamData.getString("nome");
      pos = teamData.getInt("champions");
      players = teamData.getJSONArray("rosa")
    ) {
      theory.append(new Theory(s"pos($teamName, $pos, $year).\n"))
      theory.append(new Theory(s"rosa($teamName, $players, $year).\n"))
      engine.setTheory(theory)
    }
  }

  /**
    *Return list of players bought before winning the Champions
    * @return List Of Players
    */
    def getChanpionsPlayer(): List[String]={
      return engine.solve("getChanpionsPlayer(Players).").getVarValue("Players").toList()
    }

  /**
    *Return list of players bought before winning the Champions
    * @return List Of Players
    */
  def getPlayerTeamsProlog(player: String): List[String]={
    return engine.solve(s"playerTeams(${'"'}$player${'"'} , Teams).").getVarValue("Teams").toList()
  }
  /**
    * Get All teams Name of the Player
    * @param playerName
    * @return
    */

  def getPlayerTeams(playerName: String): List[String]= {
    for(data<-soccerData;
        team <- data.getJSONArray("squadre");
        player <- team.getJSONArray("rosa") if player == playerName;
        nomeSquadra = team.getString("nome"))
      yield nomeSquadra

  }

  /**
    * Return Number Of Teams Of this Player
    * @param playerName
    * @return
    */
  def playerTeamsCounter(playerName:String): Map[String,Int] = for (
      (player, teams) <- groupPlayerWithTeams if player == playerName;
      count = teams.size
    ) yield player -> count

  /**
    * All Player Teams grouped by player Name
    * @return Set of player teamm
    */
  def groupPlayerWithTeams: Map[String, Set[String]] =
    (for(
      data <- soccerData;
      team <- data.getJSONArray("squadre");
      player <- team.getJSONArray("rosa");
      teamName = team.getString("nome")
    ) yield (player.toString, teamName)).groupBy(_._1).mapValues(_.map(_._2).toSet)

    initializeProlog();

    println("Players bought before winning champions :"+getChanpionsPlayer()+"\n")
    println("List Xherdan Teams computed with tuprolog:"+getPlayerTeamsProlog("Xherdan Shaqiri")+"\n")
    println("List Xherdan Teams computed with scala:   "+getPlayerTeams("Xherdan Shaqiri")+"\n")

  /**
    * Implicit adapter for json object, colled if o is a JSONObjcet instance
    * @param o  object in input
    * @return object converted
    */
  implicit def adapterJSONObject(o:Object): JSONObject = {
    o.asInstanceOf[JSONObject]
  }
  implicit class JSONArrayExtended(jsArray: JSONArray) {

    def foreach(joFunction: Object => Unit): Unit = map(joFunction)

    def withFilter(filter: Object => Boolean): JSONArray = {
      val array: JSONArray = new JSONArray()
      for(i<- 0 until jsArray.length())
        if(filter(jsArray.get(i)))
          array.put(jsArray.get(i))
      array
    }
    def map[T](f: Object=> T): List[T] = {
      var l: ListBuffer[T] = ListBuffer()
      for(i <- 0 until jsArray.length)
        l+=f(jsArray.get(i))
      l.toList
    }
    //prendo una lista di liste e ne creo una sola
    /**
      * given a list of list, creates on with all element using flatten
      * @param f function to use
      * @tparam T Type
      * @return List elements
      */
    def flatMap[T](f: JSONObject=>List[T]):List[T]={
      (for(i<- jsArray)
        yield f(i)).flatten
    }
  }

  /**
    * Implicit class for Term Object that adds metod toList for the VarValues and cleand
    * the result is convertion in List[String] to make easier other operation
    * @param term Prolog Term
    */
   implicit class TermExtended(term: Term){
    def toList():List[String] = {
      term.toString.drop(1).dropRight(1).split(",").toList.map(_.replaceAll("'",""))
    }
  }
}















