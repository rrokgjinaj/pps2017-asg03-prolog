%Date delle squadre che hanno partecipato alla Champions, coloro che dopo aver perso una finale acquistano dei giocatori con i quali
%nell'anno successivo la vincono. Il risultato di questo codice fornisce l'insieme di tali giocatori.

%-------------------------------------------
%teoria impostata tramite file json con scala

%(nome squadra, posizione, anno)
%pos(bayern,2,2012).
%pos(bayern,1,2013).
%pos(tottenham,2,2019).
%pos(liverpool,2,2007).
%pos(liverpool,2,2018).
%pos(liverpool,1,2019).
%
%%(nome squadra, lista giocatori, anno)
%rosa(liverpool,[salah,karius,vandijk,ajo],2018).
%rosa(liverpool,[xherdan,alisson,vandijk,salah,inzaghi],2019).
%
%rosa(bayern,[messi, sha, mbape],2012).
%rosa(bayern,[messi, sha, inzaghi],2013).
%
%rosa(milan,[kessie, baka, suso],2015).
%rosa(milan,[kessie, higuain,piontek],2016).
%-------------------------------------------

%quadre che hanno perso poi vinto all'anno successivo
%persovinto(-Squadra,-AnnoVittoria)
persovinto(S,Z):-pos(S,2,Y),Z is Y+1, pos(S,1,Z).

%ricavo tutti i giocatori dell'anno precedente e anno successivo
%oldandNewPlayers(+Squadra,+Anno,-ListaGiocatoriVecchi,-ListaGiocatoriNuovi)
oldandNewPlayers(S,Y,Lo,Ln):-findall(P,rosa(S,P,Y),[Ln]), Yo is Y-1,findall(Po,rosa(S,Po,Yo),[Lo]).

%date due liste, tolgo i giocatori vecchi dalla lista dei nuovi, (ottengo solo gli acquisti)
%sub(+GiocatoriDaTogliere, +ListaDoveTogliere, -Acquisti)
sub([],L,L).
sub([H|T],L2,R):-delete(H,L2,R1),sub(T,R1,R).


%ottengo tutti gli acquisti che mi hanno fatto vincere
%sfrutto findall per ragruppare
winningPlayers(Winners):-findall(BestPlayer,(persovinto(S,Y),oldandNewPlayers(S,Y,Lo,Ln),sub(Lo,Ln,BestPlayer)),Winners).

%giocatori acquistati prima di vincere la champions
%uso flatten per passare da una lista di liste a una lista unica
getChanpionsPlayer(P):-winningPlayers(Pl),flatten(Pl,TmP),clearList(TmP,P).

%rimuovo ripetizioni per rispondere al mio requisito

%caso base
clearList([],[]).
%uso ! per evitare le due strade, togliere e non togliere, qui toglie
clearList([H|T],L):-member(H,T),!,clearList(T,L).
%qui non toglie l'il giocatore ripetuto
clearList([H|T],[H|L]):-clearList(T,L).

%fornisce le squadre di un dato giocatore
playerTeams(P,Teams):-findall(Teams,(rosa(Teams,Ps,L),member(P,Ps)),Teams).
%conta le squadre di quel giocatore
playerTeamsCounter(P,Count,Teams):-playerTeams(P,Teams),length(Teams,Count).